class HomeController < ApplicationController
  def index
    render json: { message: "Hello from my Awesome project!" }, status: :ok
  end
end
